# 1) Escreva uma função que receba um nome e que tenha como saída uma saudação.O
# argumento da função deverá ser o nome, e saída deverá ser como a seguir:
# chamada da função: saudacao('Lalo')
# saída: 'Olá Lalo! Tudo bem com você?'


# ----------------------------------------------
def saudacao(nome):
    return f"Olá {nome}! Tudo bem com você?"

nome = input("Informe um nome: ")
print(saudacao(nome))


# ====================================================
# Exercicio 2:
# Escreva uma calculadora utilizando funções
# Ela pergunta dois numeros, e da as opções de calculo.
# (soma, diferença, multiplicação, divisão)


x = float(input("defina o valor de X: "))
y = float(input("defina o valor de Y: "))

operacao = input('Selecione a operação desejada:\n1 - Soma\n2 - Subtração\n3 - Multiplicação\n4 - Dividisão\n- ')

def soma(x, y):
    soma = x + y
    return soma

def diferença(x, y):
    diferença = x - y
    return diferença

def multiplicação(x, y):
    multiplicação = x * y
    return multiplicação

def divisão(x, y):
    divisão = x / y
    return divisão
        
if operacao == '1':
    print(soma(x,y))
elif operacao == '2':
    print(diferença(x,y))
elif operacao == '3':
    print(multiplicação(x,y))
elif operacao == '4':
    print(divisão(x,y))
else:
    print('Opcao Inválida')

# ====================================================
# Exercicio 3:
# Reescreva o exercício da quitanda do capítulo 2 separando as operações 
# em funções.


cesta = []
valor_gasto = 0

def verCesta():
    if len(cesta) < 1:
        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")
        print("A cesta está vazia")
    else:
        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")
        print(cesta)

def menuFrutas():
    global valor_gasto
    while True:
        print("""
        Menu de frutas:
        Escolha fruta desejada:
        1 - Banana   R$2.50
        2 - Melancia R$8.00
        3 - Morango  R$6.00
            
        """)

        opcao_fruta = input("Selecione a fruta para adicionar na cesta. ")

        if opcao_fruta == "1":
            cesta.append("Banana")
            valor_gasto = valor_gasto + 2.50
            if os.name == "nt":
                os.system("cls")
            else:
                os.system("clear")
            print("'Banana' foi adicionada a cesta.")
            break
        elif opcao_fruta == "2":
            cesta.append("Melancia")
            valor_gasto = valor_gasto + 8.00
            if os.name == "nt":
                os.system("cls")
            else:
                os.system("clear")
            print("'Melancia' foi adicionada a cesta.")
            break
        elif opcao_fruta == "3":
            cesta.append("Morango")
            valor_gasto = valor_gasto + 6.00
            if os.name == "nt":
                os.system("cls")
            else:
                os.system("clear")
            print("'Morango' foi adicionada a cesta.")
            break
        else:
            if os.name == "nt":
                os.system("cls")
            else:
                os.system("clear")
            print("Valor inválido, digite novamente.")

def checkout():
    print(f"""Conteúdo da cesta: {cesta}
Valor gasto: R${valor_gasto:.2f}
""")

while True:
    print("""
        Quitanda:
        1: Ver cesta
        2: Adicionar frutas
        3: Sair

        """)

    opcao = input("Selecione uma das opções acima. ")

    if opcao == "1":
        verCesta()

    elif opcao == "2":
        menuFrutas()

    elif opcao == "3":
        checkout()
        break

    else:
        print("Valor inválido, digite novamente.")


# ====================================================
# Exercicio 4:
# Escreva um programa que possua uma função que conte o
# numero de números pares passados à ela, pelo usuário.

def contaPares(lista):
    pares = 0
    for num in lista:
        if (num % 2) == 0:
            pares = pares + 1
    return pares

lista = list() 

q = int(input('Quantos valores haverá na lista ?'))
while q < 0:
    print('Erro')
    q = int(input('Quantos valores haverá na lista ?'))

for c in range(q):
    num = int(input('Valor:'))
    lista.append(num)

print('A quantidade de valores pares e:',contaPares(lista))



# ====================================================
# Exercicio 5:
# Assumindo que uma lata de tinta pinta 3m², escreva um programa
# que possua uma função que receba as dimenções de uma parede,
# passadas pelo usuario, calcule sua área, e mostre uma mensagem
# dizendo quantas latas de tinta seriam necessárias para pintar
# essa parede.

def tinta(x, y):
    area = x * y
    latas = area / 3

    return(f"Seriam necessárias {math.ceil(latas)} latas de tinta para pintar essa parede.")

altura = float(input("Qual a altura da parede em metros? "))
largura = float(input("Qual a largura da parede em metros? "))

print(tinta(altura, largura))
