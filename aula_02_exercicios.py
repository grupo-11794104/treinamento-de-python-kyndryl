# 1) Escreva um script em Python que substitua o caractere “x” por espaço considerando a
# seguinte frase:
# “Umxpratoxdextrigoxparaxtrêsxtigresxtristes”

frase =(input("digite a frase em que 'x' sera subtituido por espaços: "))
print("# ------------------------------")
print(frase.replace("x", " "))

# ======================================================================================================
# 2) Escreva um programa que receba o ano de nascimento, e que ele retorne à geração
# a qual a pessoa pertence. Para definir a qual geração uma pessoa pertence temos a
# seguinte tabela:

# Geração        Intervalo

# Baby Boomer -> Até 1964
# Geração X   -> 1965 - 1979
# Geração Y   -> 1980 - 1994
# Geração Z   -> 1995 - Atual

# Para testar se seu script está funcionando, considere os seguintes resultados esperados:

# • ano nascimento = 1988: Geração: Y
# • ano nascimento = 1958: Geração: Baby Boomer
# • ano nascimento = 1996: Geração: Z


ano = int(input("insira o ano de nascimento "))

print("sua geração é : ")
print("# ------------------------------")
if ano <= 1964 :
    print("Baby Boomer")
if ano >= 1965 and ano <= 1979 :
    print("Geração X")    
if ano >= 1980 and ano <= 1994 :
    print("Geração Y")
if ano >= 1996 :
    print("Geração Z")        
print("# ------------------------------")



# ======================================================================================================
# 3) Escreva um script em python que represente uma quitanda. O seu programa deverá
# apresentar as opções de frutas, e a cada vez que você escolher a fruta desejada, a fruta
# deverá ser adicionada a uma cesta de compras.

# Menu principal:

# Quitanda:
# 1: Ver cesta
# 2: Adicionar frutas
# 3: Sair

# Menu de frutas:
# Digite a opção desejada:
# Escolha fruta desejada:
# 1 - Banana
# 2 - Melancia
# 3 - Morango

# Digite à opção desejada: 2
# Melancia adicionada com sucesso!

# Os menus 1 e 2 deverão retornar ao menu principal após executar as suas tarefas.
# Você deverá validar as opções digitadas pelo usuário (caso ele digitar algo errado, a mensagem:
# Digitado uma opção inválida

# O programa deverá ser encerrado apenas se o usuário digitar a opção 3.


lista = []

while True: 
  
    print (f"""

    Menu principal:
    
    Quitanda:
    1: Ver cesta
    2: Adicionar frutas
    3: Sair
    """)

    opcao = int(input("Digite a opção desejada:"))

    if opcao == 1 : 
        print(lista)

    if opcao == 3 : 
        print("programa encerrado")
        break

    if opcao == 2 : 
        print(f"""

    Menu de frutas:

    Escolha fruta desejada:
    1 - Banana
    2 - Melancia
    3 - Morango
    """)
        opcao2 = int(input("Digite a opção desejada:"))

        if opcao2 == 1 :
            lista.append("Banana")
            print("Banana adcionada com sucesso")

        if opcao2 == 2 :
            lista.append("Melancia")
            print("Melancia adcionada com sucesso")

        if opcao2 == 3 :
            lista.append("Morango")
            print("Morango adcionado com sucesso")


# ======================================================================================================
# 4) Altere o exercicio numero 3 para adicionar o preço dos itens comprados, mantendo uma conta do valor
# total gasto nas compras, e no fim, imprima o valor total e os itens na cesta de compras.

lista = []
valordalista = []


while True: 

    total = sum(valordalista)

    print (f"""

    Menu principal:
    
    Quitanda:
    1: Ver cesta
    2: Adicionar frutas
    3: Sair
    """)

    opcao = int(input("Digite a opção desejada:"))

    if opcao == 1 : 
        print("# ------------------------------")
        print("seus itens no carrinho são: ", lista) 
        print("# ------------------------------")
        print("os valores respectivos dos itens são: ", valordalista)
        print("# ------------------------------")
        print("o valor total da sua compra é: ", total) 

    if opcao == 3 : 
        print("programa encerrado")
        break

    if opcao == 2 : 
        print(f"""

    Menu de frutas:

    Escolha fruta desejada:
    1 - Banana (1)
    2 - Melancia (5)
    3 - Morango (3)
    """)
        opcao2 = int(input("Digite a opção desejada:"))

        if opcao2 == 1 :
            lista.append("Banana")
            valordalista.append(1)
            print("Banana adcionada com sucesso")

        if opcao2 == 2 :
            lista.append("Melancia")
            valordalista.append(5)
            print("Melancia adcionada com sucesso")

        if opcao2 == 3 :
            lista.append("Morango")
            valordalista.append(3)
            print("Morango adcionado com sucesso")





