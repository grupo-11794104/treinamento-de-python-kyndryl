# 1) Escreva um programa em Python que simule uma dança das cadeiras. Você deverá
# importar o pacote random e iniciar uma lista com nomes de pessoas que participariam da
# brincadeira. O jogo deverá iniciar com 9 cadeiras e 10 participantes. A cada rodada,
# uma cadeira deverá ser retirada e um dos jogadores, de forma aleatória, ser eliminado. O
# jogo deverá terminar quando apenas restar uma cadeira e ao final de todas as rodadas,
# deverá ser apresentado vencedor.

# Dica: [OPCIONAL] Você poderá utilizar o modulo "time" para simular um tempo de a cada rodada para criar
# um efeito mais interessante.

# Dica: [OPCIONAL] Tentem fazer a remoção de uma pessoa aleatória por rodada sem utilizar a função "choice".



import random 
import secrets


lista=["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", "Rosangela", "Rian"]
while True:
    defora=(secrets.choice(lista))
    print(defora)
    lista.remove(defora)
    print("restaram os seguintes participantes", lista)
    if len(lista) == 1:
        break

print("o vencedor do jogo foi:", lista)

# =============================================================
# 2) Crie um programa utilizando dois arquivos, onde um deles possui todas as funcçoes utilizadas na aplicação.
# Onde o programa deverá perguntar ao usuario nome/idade de uma pessoa, e armazenar esses valores em um dicionario,
# e repetir essa ação até que a pessoa não queira mais adicionar nomes, em seguida, o programa deverá mostrar o numero
# de pessoas em categorias de acordo com a idade:
# 0-17 anos: Menor de idade
# 18-59 anos: Adulto
# 60+ anos: Idoso
# E deverá perguntar para o usuario se ele gostaria de exibir na tela uma lista com os nomes das pessoas de cada grupo,
# ou se o usuario deseja finalizar o programa.



def main():

    menores =dict()
    adultos =dict()
    idosos =dict()

    while True:
        nome = (input("digite um nome para adcionar na lista, ou digite sair para fechar: "))
        if nome == "sair" :
            print("numero de pessoas menores de idade: ", len(menores.keys()))
            print("numero de pessoas adultas: ", len(adultos.keys()))
            print("numero de pessoas idosas: ", len(idosos.keys()))
            resposta = (input("deseja ver uma lista com cada pessoa e sua respectiva idade? (S/N) : "))
            if resposta == "S" :
                print(menores)
                print(adultos)
                print(idosos)
            break         
        idade = int(input("informe a idade: "))
        if idade <= 17:
            menores.update({nome: idade})
        elif idade >= 18 and idade <= 59 :
            adultos.update({nome: idade})
        elif idade >= 60 :
            idosos.update({nome: idade})
        

if __name__ == "__main__":
     main()


# =============================================================
# 3) Crie um programa que pergunte para o usuario um numero de pessoas a participarem de um sorteio (2-20),
#  e o numero de pessoas a serem sorteadas, e depois sorteie esse numero de pessoas da lista.

# O programa deverá pegar o numero de pessoas a participar aleatoriamente desta lista:

# lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
# "Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
# "Benedito", "Tereza", "Valmir", "Joaquim"]

# Nota: A mesma pessoa não pode ganhar duas vezes.


lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
         "Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
         "Benedito", "Tereza", "Valmir", "Joaquim"]

listadesorteados = []

qtde = int(input("inform quantidade de sorteados: "))
print("# ------------------------------")
x = 0
while x < qtde :
    sorteados = lista [random.randrange ( len (lista ))]
    if sorteados in listadesorteados :
        continue 
    listadesorteados.append(sorteados)
    print("O ganhador", x+1, "foi: ", sorteados )
    x = x + 1
