# # manipulacao de arquivos

# arquivo = open("teste.txt","w") 

# #  r - read - leitura
# #  w - write escreve,(e sobrescreve)
# #  a - append - anexa (engorda)
# #  + - leitura e escrita

# conteudo = " \n Quarta linha do arquivo de texto"

# arquivo.write(conteudo)

# arquivo.close()


import csv

# with open("registro.csv","r") as arquivo:
#     conteudo = csv.reader(arquivo, delimiter=";")
#     # cabeçalho = next(conteudo)
#     # primeira_linha = next(conteudo)
#     # segunda_linha = next(conteudo)
#     # terceira_linha = next(conteudo)
#     # quarta_linha = next(conteudo)

#     lista_conteudo = []
    
#     for linha in conteudo:
#         lista_conteudo.append(linha)

#     for linha in lista_conteudo:
#         print(lista_conteudo)

with open("registro.csv","a") as arquivo:
    escrita = csv.writer(arquivo,delimiter=";")

    escrita.writerow(["44444444444","Tiago P", 27, "M", "Brasileiro"])

