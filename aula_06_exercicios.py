
# 1) Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:

# capacidade total
# capacidade atual
# placa
# modelo
# movimento

# Os comportamentos esperados para um Ônibus são:

# Embarcar
# Desembarcar
# Acelerar
# Frear

# Lembre-se que a capacidade total do ônibus é de 45 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
# de pessoas. Além disso, pessoas não podem embarcar ou desembarcar com o onibus em movimento.


class Onibus:

    def __init__ (self):
        self.__captotal = 45
        self.__capatual = 1
        self.__placa = "XPT0123"
        self.__modelo = "sei la"
        self.__movimento = True or False
        self.__velocidadetotal = 0
            
    def embarcar (self, passageiros):
        if self.__velocidadetotal == 0 :
            if self.__capatual < self.__captotal:
                self.__capatual += passageiros
                return passageiros, "embarcardos", "capacidade atual: ", self.__capatual
            return "o onibus esta lotado!"
        return "o onibus ainda esta em movimento!"
    
    def desembarcar(self, passageiros):
        if self.__velocidadetotal == 0 :
            if passageiros < self.__capatual :
                if self.__capatual > 0:
                    self.__capatual -= passageiros
                    return passageiros, "desembarcardos", "capacidade atual: ", self.__capatual
            return "nao tem", passageiros, "pessoas pra desembarcar, so tem", self.__capatual, "pessoas no onibus agora"
            return "nao tem ninguem pra desembarcar!"
        return "o onibus ainda esta em movimento!"    
    
    def acelerar(self, velocidade):
        self.__velocidadetotal += velocidade
        return "velocidade aumentada em", velocidade , "km/h"
                
    def frear(self, velocidade):
        if velocidade == 0:
            self.__movimento == False
            return "O Onibus ja esta parado!"
        self.__velocidadetotal -= velocidade
        return "velocidade reduzida em:", velocidade, "km/h"

onibusnovo = Onibus()

print (onibusnovo.embarcar(10))
print (onibusnovo.acelerar(30))
print (onibusnovo.desembarcar(5))
print (onibusnovo.acelerar(30))
print (onibusnovo.embarcar(5))
print (onibusnovo.frear(60))
print (onibusnovo.desembarcar(20))



# ==========================================================================
# 2) Implemente um programa que represente uma fila. O contexto do programa é uma
# agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra: o primeiro
# a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
# tir de números os quais representam a idade. A sua fila deverá conter os seguintes
# comportamentos:

# • Adicionar pessoa na fila: adicionar uma pessoa na fila.
# • Atender Fila: atender a pessoa respeitando a ordem de chegada
# • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila

# Class / Classes 

class Fila:

    def __init__(self):
        self.__fila = []
        self.__prioridade = []

    def adicionar(self, nome, idade):
        if idade >= 65:
            self.__prioridade.append(nome)
            self.__fila.append(nome)
            return(f"{nome} entrou na fila")
        else:
            self.__fila.append(nome)
            return(f"{nome} entrou na fila")
        
    def atender(self):
        if len(self.__fila) < 1:
            return("Não há ninguém na fila.")
        else:
            atendido = self.__fila[0]
            if self.__fila[0] in self.__prioridade:
                self.__prioridade.remove(self.__fila[0])
            self.__fila.remove(self.__fila[0])
            return(f"Atendendo a primeira pessoa da fila, {atendido}")

    def dar_prioridade(self):
        if len(self.__prioridade) < 1:
            return("Não há ninguém pra dar prioridade.")
        else:
            prioritario = self.__prioridade[0]
            self.__prioridade.remove(prioritario)
            self.__fila.remove(prioritario)
            self.__fila.insert(0, prioritario)
            return(f"{prioritario} recebeu prioridade e está na frente da fila.")
        
banco = Fila()

print(banco.adicionar("Tiago", 27))
print(banco.adicionar("Caio", 28))
print(banco.adicionar("Kaique", 30))
print(banco.adicionar("Leonardo", 20))
print(banco.adicionar("Amanda", 78))
print(banco.adicionar("Maria", 66))
print(banco.adicionar("José", 12))

print(banco.atender())
print(banco.atender())
print(banco.dar_prioridade())
print(banco.atender())
