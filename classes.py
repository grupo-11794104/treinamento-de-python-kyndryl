# Class / Classes 

class Pilha:

    def __init__ (self):
        self.__pilha = []
        self.__topo = 0
    
    def empilhar(self, item):
        self.__pilha.append(item)
        self.__topo += 1
    
    def desempilhar(self):
        if self.__topo > 0:
            ultimo_item =self.pilha[-1]
            self.__pilha.remove(ultimo_item)
            self.__topo -= 1
            return "Item removido"
        else:
            return "Nenhum item empilhado"

    def checar(self):
        return self.__pilha

balcao = Pilha()

balcao.empilhar("prato de vidro verde")
balcao.empilhar("prato de porcelana azul")
balcao.empilhar("prato de metla")
balcao.empilhar("prato de madeira")

balcao.desempilhar()
balcao.desempilhar()

# emcapsulamento

print(balcao.checar())


# Heranca

class Funcionario:

    def __init(self):
        self.__nome = ""
        self.__idade = 0
        self.__salario = 0


class Gerentes(Funcionario):

    def __init__(self):
        super().init__()
        self.__bonus = "25%"


# Polimorfismo

class Cliente:

    def __init(self):
        self.carrinho = []
        self.cpf = ''
        self.total = 0
    
    def adcionar_item(self,item):
        self.carrinho.append(item)
    
    def caixa (self):
        for item in self.carrinho:
            self.total += 1.99
        return self.total


class CleintVip(Cliente):

    def __init(self):
        super().__init__()
        self.desconto = 0.95
    
    def caixa(self):
        for item in self.carrinho:
            self.total += 1.99
        return self.total * self.desconto
    










